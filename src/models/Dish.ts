import { Orderable } from "./Orderable";

export interface Dish extends Orderable {
    startingDate: Date;
    endingDate: Date;
  }
  
  export const Dish = (
    id: number,
    name: string,
    amount: number,
    startingDate: Date,
    endingDate: Date,
    price: number
  ): Dish => ({
    id,
    name,
    amount,
    startingDate,
    endingDate,
    price
  });
  