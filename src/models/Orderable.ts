export interface Orderable {
    id: number;
    name: string;
    amount: number;
    price: number;
  }
  
  export const Orderable = (
    id: number,
    name: string,
    amount: number,
    price: number
  ): Orderable => ({
    id,
    name,
    amount,
    price
  });