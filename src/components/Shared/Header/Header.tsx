import { Link } from "react-router-dom";

export const Header = () => {

  return (
      <header className="bg-slate-500 h-24 flex">
        <div id="Left" className="w-1/3 flex justify-start">
          <Link className="h-full" to="/">
            <img className="h-full" src="./src/assets/Logo.svg" alt="Logo" />
          </Link>
          <Link className="px-5 h-full text-white font-bold text-lg flex items-center" to="/Dishes">
            <p>
              Dishes
            </p>
          </Link>
          <Link className="px-5 h-full text-white font-bold text-lg flex items-center" to="/Drinks">
            <p>
              Drinks
            </p>
          </Link>
          <Link className="px-5 h-full text-white font-bold text-lg flex items-center" to="/Cart">
            <p>
              Cart
            </p>
          </Link>
        </div>
        <div id="Center" className="w-1/3 flex justify-center items-center">
          <h1 className="text-white text-7xl font-bold">Better Food</h1>
        </div>
        <div id="Right" className="w-1/3 flex justify-end">
          <Link className="px-5 h-full" to="/Account">
            <img className="h-full" src="./src/assets/Account.svg" alt="Account" />
          </Link>
          <Link className="px-5 h-full" to="/LogOut">
            <img className="h-full" src="./src/assets/LogOut.svg" alt="LogOut" />
          </Link>
        </div>
      </header>
  );
};
