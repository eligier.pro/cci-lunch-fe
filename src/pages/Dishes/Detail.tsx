import { Title } from "../../components/Shared/Title/Title";
import { useDish } from "./DishesModel";
import { Dish } from "../../models/Dish";

export const Detail = () => {
    const dish: Dish | null = useDish();

  return (
    <>
        {dish &&
            <>
                <Title title={dish.name} />
                <div className="grid grid-cols-4 gap-4">

                </div>
            </>
        }
    </>
  );
};
