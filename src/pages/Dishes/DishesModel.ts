import axios from "axios";
import { Dish } from "../../models/Dish";
import { useEffect, useState } from "react";
import { API_BASE } from '../../Const';
import { useParams } from "react-router-dom";

const BASE_ROOT: string = API_BASE + 'Dish';

export const useDishes = () => {
    const [dishes, setDishes] = useState<Dish[]>([]);

    useEffect(() => {
        axios.get(BASE_ROOT)
          .then((response) => {
            setDishes(response.data);
          })
          .catch((err) => {
            console.log(err);
          });
      }, []);

    return dishes;
}

export const useDish = () => {
  const [dish, setDish] = useState<Dish | null>(null);
  const { id } = useParams<{ id: string }>();
   
  useEffect(() => {
      axios.get(BASE_ROOT + '/' + id)
        .then((response) => {
          setDish(response.data);
        })
        .catch((err) => {
          console.log(err);
        });
    });

  return dish;
}