import { Title } from "../../components/Shared/Title/Title";
import { OrderableCard } from "../../components/OrderableCard/OrderableCard";
import { useDishes } from "./DishesModel";
import { Dish } from "../../models/Dish";

export const Dishes = () => {
  const dishes: Dish[] = useDishes();

  return (
    <>
      <Title title={"Dishes"} />

      <div className="grid grid-cols-4 gap-4">
        {dishes && dishes.map((dish) => (
          <OrderableCard
            orderable={dish}
            type="Dish"
            key={dish.id}
          />
        ))}
      </div>
    </>
  );
};
