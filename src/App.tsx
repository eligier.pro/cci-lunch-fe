import { BrowserRouter } from "react-router-dom";
import { Template } from "./components/Shared/Template/Template";
import axios from 'axios';

function App() {
  axios.get('https://localhost:7216/Dish')
  .then(response => {
    console.log(response.data);
  })
  .catch(error => {
    console.error(error);
  });

  return (
    <BrowserRouter>
      <Template />
    </BrowserRouter>
  )
}

export default App;
