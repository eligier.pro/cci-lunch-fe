import axios from 'axios';
import { API_BASE } from '../../../Const';

const BASE_ROOT: string = API_BASE + 'register';

export const registerUser = async (email: string, password: string, confirmPassword: string) => {
    if (password === confirmPassword) {
        await axios.post(BASE_ROOT, { email, password })
        .then((response) => {
            console.log(response);
        })
        .catch((err) => {
            console.log(err.response.data.errors);
        });
    } else {
        console.log('Missmatch password and confirmation');
    }
};