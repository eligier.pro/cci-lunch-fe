import { Route, Routes } from "react-router-dom";
import { HomePage } from "../../../pages/Home-page/Home-page";
import { Dishes } from "../../../pages/Dishes/Dishes";
import { Drinks } from "../../../pages/Drinks/Drinks";
import { Header } from "../Header/Header";
import { Authentication } from "../../../pages/Authentication/Authentication";
import { Cart } from "../../../pages/Cart/Cart";
import { Doing } from "../../../pages/Doing/Doing";
import { Footer } from "../Footer/Footer";
import { Detail as DishDetail } from "../../../pages/Dishes/Detail";

export const Template = () => {
  return (
      <div className="bg-gray-50 flex flex-col h-screen">
        <Header />
        <main  className="w-3/4 mx-auto my-20 flex-grow">
          <Routes>
            <Route path="/" element={<HomePage />} />
            <Route path="/Dishes" element={<Dishes />} />
            <Route path="/Dishes/:id" element={<DishDetail />} />
            <Route path="/Drinks" element={<Drinks />} />
            <Route path="/Drinks/:id" element={<Doing />} />
            <Route path="/Cart" element={<Cart />} />
            <Route path="/Account" element={<Authentication />} />
          </Routes>
        </main >
        <Footer />
      </div>
  );
};
