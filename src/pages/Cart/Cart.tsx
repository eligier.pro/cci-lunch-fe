import { CartItem } from "../../models/CartItem";
import { Title } from "../../components/Shared/Title/Title";
import { useCartItems } from "./CartItemsModel";

export const Cart = () => {
  const cartItems: CartItem[] = useCartItems();

  return (
    <>
      <Title title={"Cart"} />

      <div className="grid grid-cols-4 gap-4">
        {cartItems && cartItems.map((cartItem) => (
          <>
            <p>{cartItem.custommerId}</p>
            <p>{cartItem.orderableId}</p>
            <p>{cartItem.amount}</p>
          </>
        ))}
      </div>
    </>
  );
};