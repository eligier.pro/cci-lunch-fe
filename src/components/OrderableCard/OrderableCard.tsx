import { Link } from "react-router-dom";
import { Orderable } from "../../models/Orderable";

interface Props {
  orderable: Orderable;
  type: string;
}

export const OrderableCard = ({ orderable, type }: Props) => {

  return (
    <Link
      to={`${orderable.id}`}
      className="w-full"
    >
      <img
        src={`src/assets/${type}.png`}
        alt="Default picture"
        className="w-full h-60 rounded-t-2xl  bg-white"
      />
      <div className="w-full h-60 p-3.5 rounded-b-2xl  bg-white">
        <h3 className="text-center font-bold">{orderable.name}</h3>
        <div className="grid grid-cols-2 items-center">
          <p className="font-bold">{orderable.price} €</p>
          <div className="flex justify-end items-center text-center bg-slate-500 rounded-full text-2xl font-bold text-white h-10">
            <p className="w-1/3">+</p>
            <p className="w-1/3">0</p>
            <p className="w-1/3">-</p>
          </div>
        </div>
      </div>
    </Link>
  );
};
