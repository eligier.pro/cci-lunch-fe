import axios from "axios";
import { Drink } from "../../models/Drink";
import { useEffect, useState } from "react";
import { API_BASE } from '../../Const';

const BASE_ROOT: string = API_BASE + 'Drink';

export const useDrinks = () => {
    const [drinks, setDrinks] = useState<Drink[]>([]);

    useEffect(() => {
        axios.get(BASE_ROOT)
          .then((response) => {
            setDrinks(response.data);
          })
          .catch((err) => {
            console.log(err);
          });
      }, []);

    return drinks;
}