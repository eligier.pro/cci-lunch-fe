import { Login } from "../../components/Authentication/Login/Login";
import { Register } from "../../components/Authentication/Register/Register";
import { Title } from "../../components/Shared/Title/Title";

export const Authentication = () => {

  return (
    <>
      <Title title={"Authentication"} />
      
      <div className="flex">
        <Login />

        <Register />
      </div>
      
    </>
  );
};