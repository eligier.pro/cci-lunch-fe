import { Orderable } from "./Orderable";

export interface Drink extends Orderable {
    capacity: number;
  }
  
  export const Drink = (
    id: number,
    name: string,
    amount: number,
    capacity: number,
    price: number
  ): Drink => ({
    id,
    name,
    amount,
    capacity,
    price
  });
  