export interface CartItem {
    custommerId: number;
    orderableId: number;
    amount: number;
  }
  
  export const CartItem = (
    custommerId: number,
    orderableId: number,
    amount: number,
  ): CartItem => ({
    custommerId,
    orderableId,
    amount,
  });
  