import axios from 'axios';
import { API_BASE } from '../../../Const';

const BASE_ROOT: string = API_BASE + 'login';

export const loginUser = async (email: string, password: string) => {
    await axios.post(BASE_ROOT, { email, password })
        .then((response) => {
            console.log(response);
        })
        .catch((err) => {
            console.log(err);
        });
};