import { Title } from "../../components/Shared/Title/Title";

export const HomePage = () => {
  return (
    <Title title={"CCI Lunch"} />
  );
};
