import { Drink } from "../../models/Drink";
import { OrderableCard } from "../../components/OrderableCard/OrderableCard";
import { Title } from "../../components/Shared/Title/Title";
import { useDrinks } from "./DrinksModel";

export const Drinks = () => {
  const drinks: Drink[] = useDrinks();

  return (
    <>
      <Title title={"Drinks"} />

      <div className="grid grid-cols-4 gap-4">
        {drinks && drinks.map((drink) => (
          <OrderableCard
            orderable={drink}
            type="Drink"
            key={drink.id}
          />
        ))}
      </div>
    </>
  );
};