import axios from "axios";
import { CartItem } from "../../models/CartItem";
import { useEffect, useState } from "react";
import { API_BASE } from '../../Const';

const BASE_ROOT: string = API_BASE + '';

export const useCartItems = () => {
  const [cartItems, setCartItems] = useState<CartItem[]>([]);

    useEffect(() => {
        axios.get(BASE_ROOT)
          .then((response) => {
            setCartItems(response.data);
          })
          .catch((err) => {
            console.log(err);
          });
      }, []);

    return cartItems;
}