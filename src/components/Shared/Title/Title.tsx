interface Props {
  title: string;
  subtitle?: string;
}

export const Title = ({ title, subtitle }: Props) => {
  return (
    <>
      <h1 className="text-4xl mb-10">{title}</h1>

      {subtitle && <h2 className="text-3xl mb-10">{subtitle}</h2>}
    </>
  );
};
