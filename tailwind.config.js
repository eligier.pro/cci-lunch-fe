/** @type {import('tailwindcss').Config} */
export default {
  content: [
    "./index.html",
    "./src/**/*.{jsx,tsx,js,ts}",
  ],
  theme: {
    extend: {
      width: {
        "76": "19rem"
      },
      height: {
        "104": "28rem"
      }
    },
  },
  plugins: [],
}